package com.jz.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ServletDemo1 class
 *
 * @author zhangjun
 * @date 2018/4/4
 */
public class ServletDemo1 extends HttpServlet {

    @Override
    public void init(ServletConfig servletConfig) throws ServletException{
        System.out.println("Servlet Start");
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        System.out.println("Call servlet");
    }

    @Override
    public void destroy() {
        System.out.println("Destroy servlet");
    }

    public ServletConfig getservletConfig(){
        return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        ServletContext context = this.getServletContext();
        String param = context.getInitParameter("username");
        System.out.println(param);
        String getRealPath = context.getRealPath("/resource/pop_team_epic_by_ucantw1n-dasexa0.gif");
        System.out.println(getRealPath);
    }
}
