package com.jz.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletCount extends HttpServlet {
    @Override
    public void init() throws ServletException{
        Integer count = 1;
        this.getServletContext().setAttribute("count",count);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        ServletContext context = this.getServletContext();
        Integer count = (Integer)context.getAttribute("count");
        context.setAttribute("count",count+1);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write("Count: " + count);
    }
}
